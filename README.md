## Recommendation system for Testbook users.

### Introduction

This project aims to explore different methods to build custom course recommendation system for Testbook based on previous user test performance after enrolling in various courses.

Presentation Link : https://docs.google.com/presentation/d/10g2eGSRcaAidsUe1nVa62wDDmrW1hyoLgm1Vim-AVNo/edit?usp=sharing

Code Link : https://bitbucket.org/sapsa/decipher/src/master/

Working Model : Work in progress due to shortage of time 

**Folder Structure**
├── **app**
│   ├── app.py
│   ├── config.py
│   ├── Procfile
│   └── requirements.txt
├── **Docs**
├── **Notebooks**
│   ├── data
│   │   ├── class_details.json
│   │   ├── course_dict.json
│   │   ├── course_enrollments.json
│   │   ├── students.json
│   │   ├── tests_details.json
│   │   └── test_submissions.json
│   ├── dataset.csv
│   ├── export_dataframe.csv
│   ├── jsondata.json
│   ├── Decipher.ipynb
│   ├── modified.csv
│   └── Preliminary_Analysis.ipynb
└── README.md

app : Working Model

Docs: Presentation Files

**Notebooks: Code Files**

1. **Preliminary_Analysis.ipynb** - Contains EDA related codes

2. **Decipher.ipynb** - Contains Recommendation System related codes

   

### Data-set Results and Statistics

1. **The number of unique users is :-** 10,000
2. **Number of Students Enrolled in a course  :-** 198
3. **Number of unique courses  (target) (Based on Class Details Data-set):-** 42
4. **Total Number of courses category (target_super_group)  (Based on Class Details Data-set) :-** 9 
5. **Total Number of Course :-** 219
6. **No. of Students appeared for a test :-** 3589

